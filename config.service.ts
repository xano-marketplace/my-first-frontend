import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	components: any,
	instructions: any;
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})

export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>('https://xoxf-hd6x-6qgg.n7.xano.io/api:fizFrpvf');
	public config: XanoConfig = {
		title: 'My First Front End',
		summary: 'Simple demo of slack webhook integration into Xano.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/slack-webhooks',
		components: [],
		instructions: [
			'Install the extension in your Xano Workspace',
			`In your Slack workspace create an app and enable <b>incoming webhooks</b>.
			<a href="https://api.slack.com/messaging/webhooks" target="_blanks">Detailed Instructions Here</a>`,
			`Copy your slack webhook URL and set it as your <b>slack_webhook_url</b> in your Xano workspace`,
			'In your Xano workspace go to the newly added Slack API Group and copy your API BASE URL',
			'Paste this in as "Your Xano API URL"'
		],
		descriptionHtml: ``,
		logoHtml: '',
		requiredApiPaths: []
	};

	constructor(
		private apiService: ApiService,
		private xanoService: XanoService,
		private snackBar: MatSnackBar) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {Accept: 'text/yaml'},
			responseType: 'text',
		});
	}

	public showErrorSnack(error) {
		this.snackBar.open(get(error, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
	}

}
